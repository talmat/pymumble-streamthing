#!/bin/sh

if [ -f activate_venv ]; then
  rm activate_venv
fi

if [ -d .pymumble_venv ]; then
  rm -rf .pymumble_venv
fi

if [ -d pymumble/build ]; then
  rm -rf pymumble/build
fi
