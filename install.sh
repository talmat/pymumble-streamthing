#!/bin/sh

venv_path=".pymumble_venv"

if [ ! -d ${venv_path} ]; then
  mkdir ${venv_path}
  python3 -m venv ${venv_path}
fi

source ${venv_path}/bin/activate
pip3 install --upgrade pip
pushd pymumble
pip3 install -r requirements.txt
python3 setup.py install
popd

echo "source ${venv_path}/bin/activate" > activate_venv
