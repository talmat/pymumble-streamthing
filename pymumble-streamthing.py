#!/usr/bin/env python3

import sys, os, time, atexit, stat
import configparser
import pymumble_py3

if len(sys.argv) < 2:
    print(f'Usage: {sys.argv[0]} config')
    sys.exit(1)

print(f'Read configuration from {sys.argv[1]}')
c = configparser.ConfigParser()
c.read(sys.argv[1])

host = c['server']['host']
port = int(c['server']['port'])
password = c['server'].get('password', '')

nick = c['client']['nick']
channelname = c['client'].get('join_channel', None)
certpath = c['client']['certpath']
certfile = c['client']['certfile']
keyfile = c['client']['keyfile']

pipe_name = c['fifo']['pipe_name']
stream_bandwidth = int(c['audio']['stream_bandwidth'])
fragment_length = float(c['audio']['fragment_length'])
fragment_readahead = int(c['audio']['fragment_readahead'])
min_buffer_level = float(c['audio']['min_buffer_level'])

# Mumble audio parameters (don't change these)
samplerate = 48000
channels = 1
bytespersample = 2

# Spin up a client and connect to mumble server
print(f'Connecting to {host}... ', end='')
mumble = pymumble_py3.Mumble(host, nick, port=port, password=password, certfile=f'{certpath}/{certfile}', keyfile=f'{certpath}/{keyfile}', reconnect=True)
mumble.set_application_string("pymumble-streamthing")
mumble.set_codec_profile("audio")
mumble.set_receive_sound(0)     # Disable receiving sound from mumble server
mumble.set_loop_rate(0.01)
mumble.start()
mumble.is_ready()               # Wait for client ready
print('connected')

mumble.set_bandwidth(stream_bandwidth)

if channelname is not None:
    try:
        channel = mumble.channels.find_by_name(channelname)
        channel.move_in()
    except pymumble_py3.errors.UnknownChannelError:
        print(f'Channel {channelname} not found!')
    else:
        print(f'Moved to channel {channelname}')

mumble.users.myself.unmute()    # Make sure the user is not muted

# Create named pipe if it doesn't exist already
if not os.path.exists(pipe_name):
    os.mkfifo(pipe_name, mode=0o600)
    print(f'Named pipe {pipe_name} created.')
else:
    print(f'{pipe_name} already exists. Leaving.')
    sys.exit(1)

# Register atexit handler to remove fifo when leaving
def remove_fifo(path):
    if stat.S_ISFIFO(os.stat(path).st_mode):
        os.remove(path)
atexit.register(remove_fifo, pipe_name)

# Open named pipe for receiving audio data
with open(pipe_name, 'rb') as input:
    # Begin processing received data and send it to the mumble server
    fragment_size = int(channels*samplerate*bytespersample*fragment_length)
    sleep_time = (1-min_buffer_level)*fragment_readahead*fragment_length
    filled_until = time.time()
    while True:
        now = time.time()
        buffer_until = now + fragment_readahead * fragment_length
        fragments = int((buffer_until - filled_until) / fragment_length)
        data = input.read(fragments * fragment_size)
        datalen = len(data)

        if (datalen > 0) and (datalen < fragments*fragment_size):
            usable = int(datalen/fragment_size)
            print(f'fragments(bytes) expected: {fragments}({fragments*fragment_size}), got: {usable}({usable*fragment_size})')
            data = bytes(data[:usable*fragment_size])
            filled_until += usable * fragment_length
        else:
            filled_until += fragments * fragment_length

        if data != b'':
            mumble.sound_output.add_sound(data)

        time.sleep(sleep_time)
